import { Inter } from "@next/font/google";
import Banner from "../@components/Banner/Banner";
import Blog from "../@components/Blog/Blog";
import FAQ from "../@components/FAQ/FAQ";
import Kitchen from "../@components/Kitchen/Kitchen";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    // root page
    <>
      <Banner />
      <Kitchen />
      <Blog />
      <FAQ />
    </>
  );
}
