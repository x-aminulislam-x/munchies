import Footer from "../@components/Footer/Footer";
import Navbar from "../@components/Navbar/Navbar";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    // layout for all page
    <main>
      <Navbar />
      {children}
      <Footer />
    </main>
  );
}
