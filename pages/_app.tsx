import "bootstrap/dist/css/bootstrap.min.css"; // Import bootstrap CSS
import type { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";
import { Provider } from "react-redux";
import store from "../store/store";
import "../styles/globals.scss";
import RootLayout from "./layout";

export default function App({ Component, pageProps }: AppProps) {
  const queryClient = new QueryClient();

  return (
    // QueryClientProvider wrapper for reactQuery
    <QueryClientProvider client={queryClient}>
      {/*  Provider wrapper for Redux */}
      <Provider store={store}>
        <RootLayout>
          <Component {...pageProps} />
        </RootLayout>
      </Provider>
    </QueryClientProvider>
  );
}
