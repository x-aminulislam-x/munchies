import Link from "next/link";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import { ProductsService } from "../../@services/api/products.service";

interface IForm {
  email: string;
  phone: number;
  address: string;
  name: string;
}

const PlaceOrder = () => {
  const { orders } = useSelector((state: any) => state.orders);

  const [isLoading, setIsLoading] = useState(false);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: IForm) => {
    setIsLoading(true);
    const total = orders
      .map((item: any) => item.price * item.quantity_addToCart)
      .reduce((curr: number, pre: number) => curr + pre);

    const vat = orders
      .map(
        (item: any) => ((item.price * item.quantity_addToCart) / 100) * item.vat
      )
      .reduce((curr: number, pre: number) => curr + pre);

    // remove unnecessary properties

    const items = orders.map(
      ({ quantity_addToCart, ...keepAttrs }: any) => keepAttrs
    );

    // preparing
    const requireOBJ = {
      email: data.email,
      customer: { name: data.name, address: data.address, phone: data.phone },
      calculation: {
        price: total,
        vat: vat,
        total: total + vat,
      },
      items: items,
    };

    ProductsService.order(requireOBJ)
      .then((res) => {
        Swal.fire("Thank You!", "You order has been confirmed!", "success");
        window.location.replace("/");
      })
      .catch((err) => Swal.fire("Sorry!", "Something went wrong!", "error"))
      .finally(() => setIsLoading(false));
  };
  return (
    <div className="place-order-main">
      <div className="place-order-wrapper container">
        {!!orders.length ? (
          <>
            <table className="table mt-5">
              <thead>
                <tr className="d-flex justify-content-between">
                  <th scope="col">Id</th>
                  <th scope="col">Image</th>
                  <th scope="col">Name</th>
                  <th scope="col">Quantity</th>
                </tr>
              </thead>
              <tbody>
                {orders.map((item) => {
                  return (
                    <tr
                      key={item.id}
                      className="d-flex justify-content-between align-items-center"
                    >
                      <th scope="row">{item.id}</th>
                      <td>
                        <img width={50} height={50} src={item.image} />
                      </td>
                      <td>{item.name}</td>
                      <td>{item.quantity_addToCart}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <p className="text-center text-success mt-5">
              Please fill the form to confirm the order
            </p>
            <form
              className="w-50 m-auto border border-2 rounded p-3 mt-5"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="d-flex flex-column  my-3">
                <input
                  className="form-control"
                  {...register("name", { required: true })}
                  placeholder="Name"
                />
                {errors.name && (
                  <span className="text-danger">This field is required</span>
                )}
              </div>
              <div className="d-flex flex-column  my-3">
                <input
                  className="form-control"
                  {...register("address", { required: true })}
                  placeholder="Address"
                />
                {errors.address && (
                  <span className="text-danger">This field is required</span>
                )}
              </div>
              <div className="d-flex flex-column  my-3">
                <input
                  className="form-control"
                  {...register("phone", { required: true })}
                  placeholder="Phone"
                  type="number"
                />
                {errors.phone && (
                  <span className="text-danger">This field is required</span>
                )}
              </div>
              <div className="d-flex flex-column my-3">
                <input
                  className="form-control"
                  type="email"
                  {...register("email", { required: true })}
                  placeholder="Email"
                />
                {errors.email && (
                  <span className="text-danger">This field is required</span>
                )}
              </div>
              <input
                disabled={isLoading}
                className="btn btn-outline-primary m-auto d-block"
                type="submit"
              />
            </form>
          </>
        ) : (
          <div className="my-5">
            <h1 className="text-center">No Product added in cart </h1>
            <button className="btn btn-outline-secondary text-white m-auto d-block">
              <Link href="/" className="text-decoration-none">
                go to Home
              </Link>
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default PlaceOrder;
