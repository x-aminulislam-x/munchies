import blogImg1 from "@assets/images/unsplash_A.svg";
import blogImg2 from "@assets/images/unsplash_B.svg";
import blogImg3 from "@assets/images/unsplash_C.svg";
import blogImg4 from "@assets/images/unsplash_D.svg";
import blogImg5 from "@assets/images/unsplash_E.svg";

export const blogs = [
  {
    id: 1,
    image: blogImg1,
    title: "Most Satisfying Cake decorating Cake ideas ",
    description:
      "Quis hendrerit nibh mauris sed faucibus. Quis hendrerit nibh mauris sed faucibus.",
  },
  {
    id: 2,
    image: blogImg2,
    title: "Most Satisfying Cake decorating Cake ideas ",
    description:
      "Quis hendrerit nibh mauris sed faucibus. Quis hendrerit nibh mauris sed faucibus.",
  },
  {
    id: 3,
    image: blogImg3,
    title: "Most Satisfying Cake decorating Cake ideas ",
    description:
      "Quis hendrerit nibh mauris sed faucibus. Quis hendrerit nibh mauris sed faucibus.",
  },
  {
    id: 4,
    image: blogImg4,
    title: "Most Satisfying Cake decorating Cake ideas ",
    description:
      "Quis hendrerit nibh mauris sed faucibus. Quis hendrerit nibh mauris sed faucibus.",
  },
  {
    id: 5,
    image: blogImg5,
    title: "Most Satisfying Cake decorating Cake ideas ",
    description:
      "Quis hendrerit nibh mauris sed faucibus. Quis hendrerit nibh mauris sed faucibus.",
  },
];
