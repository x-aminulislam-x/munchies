import Image from "next/image";
import { blogs } from "./blog";

const Blog = () => {
  return (
    <div className="blog-main my-5">
      <div className="blog-wrapper container">
        <p className="title">Our Blog</p>
        <div className="blog-items my-5">
          {blogs.map((item) => {
            return (
              <div key={item.id} className="blog-card">
                <div className="blog-img">
                  <Image src={item.image} alt="" />
                </div>
                <div className="blog-dtls">
                  <p>{item.title}</p>
                  <span>{item.description}</span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Blog;
