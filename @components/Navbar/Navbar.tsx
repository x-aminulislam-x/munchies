import cartIcon from "@assets/icons/cart.svg";
import searchIcon from "@assets/icons/search.svg";
import logoImg from "@assets/images/logo.svg";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const Navbar = () => {
  const { orders }: any = useSelector((state: any) => state.orders);
  const [total, setTotal] = useState<number>(0);

  const pathName = usePathname();

  useEffect(() => {
    if (!orders.length) return;
    setTotal(0);
    // calculating total how many items added in cart
    orders.map((item) =>
      setTotal((state) => (state += item.quantity_addToCart))
    );
  }, [orders]);

  return (
    <div className="navbar-main bg-dark">
      <div className="container">
        <nav className="navbar navbar-light container-fluid">
          <div className="">
            <Link href="/" className="navbar-brand">
              <Image src={logoImg} alt="" />
            </Link>
          </div>
          <div className="nav-routes">
            <ul className="d-flex  list-item">
              <li className="pe-2">
                <Link href="/">Home</Link>
              </li>
              <li className="px-4">
                <Link href="">About</Link>
              </li>
              <li className="px-4">
                <Link href="">Menu</Link>
              </li>
              <li className="px-4">
                <Link href="">Blog</Link>
              </li>
              <li className="ps-4">
                <Link href="">Contact</Link>
              </li>
            </ul>
          </div>
          <div className="nav-others">
            <Link href="" className="me-3">
              <Image src={searchIcon} alt="" />
            </Link>
            {pathName === "/placeorder" || (
              <Link href="/placeorder" className="add-to-cart-icon ms-2">
                <Image src={cartIcon} alt="" />
                {total <= 0 || (
                  <span className="bg-primary text-white">{total}</span>
                )}
              </Link>
            )}
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
