import plusIcon from "@assets/icons/addToCart.svg";
import starIcon from "@assets/icons/star.svg";
import Image from "next/image";
import { useQuery } from "react-query";
import { useDispatch } from "react-redux";
import { addToCart } from "../../store/reducers/ordersReducer";
import { fetchProducts } from "../../store/reducers/productsReducer";

const Kitchen = () => {
  const dispatch = useDispatch();
  const { isLoading, error, data } = useQuery("repoData", () =>
    dispatch(fetchProducts())
  );

  if (isLoading)
    return <h3 className="text-info text-center my-5">Loading...</h3>;

  return (
    <div className="kitchen-main">
      <div className="kitchen-wrapper container">
        <div className="kitchen-topper">
          <p className="title">Home Kitchen</p>
          <div className="btn-group mt-4">
            <input
              type="radio"
              className="btn-check"
              name="radioBtn"
              id="checks1"
              defaultValue=""
              autoComplete="off"
            />
            <label
              className="btn btn-outline-primary px-4 py-2"
              htmlFor="checks1"
            >
              All
            </label>
            <input
              type="radio"
              className="btn-check"
              name="radioBtn"
              id="checks2"
              defaultValue=""
              autoComplete="off"
            />
            <label
              className="btn btn-outline-primary px-4 py-2"
              htmlFor="checks2"
            >
              Button
            </label>
            <input
              type="radio"
              className="btn-check"
              name="radioBtn"
              id="checks3"
              defaultValue=""
              autoComplete="off"
            />
            <label
              className="btn btn-outline-primary px-4 py-2"
              htmlFor="checks3"
            >
              Free Delivery
            </label>
            <input
              type="radio"
              className="btn-check"
              name="radioBtn"
              id="checks4"
              defaultValue=""
              autoComplete="off"
            />
            <label
              className="btn btn-outline-primary px-4 py-2"
              htmlFor="checks4"
            >
              New
            </label>
            <input
              type="radio"
              className="btn-check"
              name="radioBtn"
              id="checks5"
              autoComplete="off"
              defaultValue=""
            />
            <label
              className="btn btn-outline-primary px-4 py-2"
              htmlFor="checks5"
            >
              Coming
            </label>
          </div>
          <div className="mt-4 w-25">
            <select
              className="form-select form-select-lg mb-3 text-secondary"
              aria-label=".form-select-lg example"
              defaultValue={1}
            >
              <option defaultValue={1} selected>
                Filters
              </option>
              <option defaultValue={1}>One</option>
              <option defaultValue={2}>Two</option>
              <option defaultValue={3}>Three</option>
            </select>
          </div>
        </div>
        <div className="kitchen-items">
          {data?.payload?.map((item, indx) => {
            return (
              <div key={item?.id} className="food-card">
                <div className="food-img">
                  <img
                    src={item?.image}
                    alt=""
                    // TODO:: handle the event for image breaking

                    // onError={({ currentTarget }: any) => {
                    //   currentTarget.onerror = null; // prevents looping
                    //   currentTarget.src = forBrokenImg;
                    // }}
                  />
                  <span className="bg-primary p-1">{item?.vat}%</span>
                </div>
                <div className="food-dtls">
                  <div className="name-part">
                    <span className="food-name">{item?.name}</span>
                    <span className="food-price">${item?.price}</span>
                  </div>
                  <div className="others-part d-flex align-items-center mt-3">
                    <div className="food-rating d-flex align-items-center">
                      <Image src={starIcon} alt="" />
                      <span className="ms-1">
                        4.7-{item.quantity_available}
                      </span>
                    </div>
                    <div className="food-delivery-time">50-79 min</div>
                    <div
                      onClick={() => dispatch(addToCart(item))}
                      className="food-add-to-cart ms-auto"
                    >
                      <Image src={plusIcon} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <button className="load-btn d-flex btn btn-outline-primary text-secondary ms-auto me-auto">
          + Load More...
        </button>
      </div>
    </div>
  );
};

export default Kitchen;
