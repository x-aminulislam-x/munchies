import foodDeliveryImg from "@assets/images/food.svg";
import Image from "next/image";

const FAQ = () => {
  return (
    <div className="faq-main">
      <div className="faq-wrapper container d-flex justify-content-between">
        <div className="faq-form w-75 h-100">
          <div className="">
            <h3 className="text-large">
              Do you have a question or want to become a seller?
            </h3>
            <span>
              Fill this form and our manager will contact you next 48 hours.
            </span>
          </div>
          <form action="" className="my-2">
            <div className="d-flex ">
              <input
                type="text"
                className=" form-control"
                placeholder="Your Name"
              />
              <input
                type="text"
                className=" form-control ms-2"
                placeholder="Your e-mail"
              />
            </div>
            <textarea
              className="form-control mt-2 h-100"
              placeholder="your message"
            />
          </form>
        </div>
        <div className="faq-image ms-3">
          <Image src={foodDeliveryImg} alt="" />
        </div>
      </div>
    </div>
  );
};

export default FAQ;
