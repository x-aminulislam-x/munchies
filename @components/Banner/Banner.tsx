import cookingImg from "@assets/images/cooking.svg";
import Image from "next/image";

const Banner = () => {
  return (
    <div className="banner-main bg-dark ">
      <div className="banner-wrapper d-flex flex-row container">
        <div className="banner-text p-5">
          <h1 className="text-white">Authentic Home food in UK</h1>
          <p className="text-white">
            What2Eat is a courier service in which authentic home cook food is
            delivered to a customer.
          </p>
          <div className="banner-input">
            <input placeholder="Search food you love" />
            <button className="bg-primary text-white">search</button>
          </div>
        </div>
        <div className="banner-img p-3">
          <Image src={cookingImg} alt="cooking" />
        </div>
      </div>
    </div>
  );
};

export default Banner;
