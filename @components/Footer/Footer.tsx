import fbIcon from "@assets/icons/facebook.svg";
import instaIcon from "@assets/icons/instagram.svg";
import logoImg from "@assets/icons/what2eat.svg";
import Image from "next/image";
import Link from "next/link";

const Footer = () => {
  return (
    <div className="footer-main mt-5 ">
      <div className="container">
        <nav className="footer d-flex justify-content-between align-items-center container-fluid py-4">
          <div className="">
            <Link href="/" className="footer-brand">
              <Image src={logoImg} alt="" />
            </Link>
          </div>
          <div className="footer-routes">
            <ul className="d-flex  list-item">
              <li className="pe-2">
                <Link href="/">Home</Link>
              </li>
              <li className="px-4">
                <Link href="">About</Link>
              </li>
              <li className="px-4">
                <Link href="">Menu</Link>
              </li>
              <li className="px-4">
                <Link href="">Blog</Link>
              </li>
              <li className="ps-4">
                <Link href="">Contact</Link>
              </li>
            </ul>
          </div>
          <div className="footer-others">
            <Link href="" className="me-3 bg-white pb-2 pt-1 px-2 rounded">
              <Image src={fbIcon} alt="" />
            </Link>
            <Link
              href=""
              className="add-to-cart-icon bg-white ms-2 pt-1 pb-2 px-2 rounded"
            >
              <Image src={instaIcon} alt="" />
            </Link>
          </div>
        </nav>
      </div>
      <div className="footer-border"></div>
      <span className="d-block py-3 text-white text-center">
        Copyright @2021 What2Eat
      </span>
    </div>
  );
};

export default Footer;
