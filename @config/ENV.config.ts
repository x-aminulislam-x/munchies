interface IENV {
  ApiEndPoint: string;
}

// env configuration
export const ENV: IENV = {
  ApiEndPoint: process.env.NEXT_PUBLIC_BASE_URL,
};
