import { combineReducers } from "@reduxjs/toolkit";
import ordersReducer from "./reducers/ordersReducer";
import productsReducer from "./reducers/productsReducer";

// interface for reducers
export interface SessionState {
  products: any;
  orders: any;
}

// combining the reducers for multiple reducers
const rootReducer = combineReducers<SessionState>({
  products: productsReducer,
  orders: ordersReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
