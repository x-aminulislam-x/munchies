import { apiIns } from "../../@config/api.config";

// making service for api call
export const ProductsService = {
  // method for get the products
  getProducts: async (): Promise<any> => await apiIns.get("products"),
  // method for submit orders
  order: async (payload: any): Promise<any> =>
    await apiIns.post("order", payload, {
      headers: {
        "x-access-user": payload.email,
      },
    }),
};
